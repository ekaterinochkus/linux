* Systemd
Щоб описати це в загальному вигляді, Systemd System and Service Manager використовується для запуску чогось.
Щось називається юнітами (unit). Існує кілька типів юнітів. Одним з найважливіших типів юнітів є сервіс (service).

~systemctl -t help~ - вивести всі можливі типи юнітів.

Основна перевага роботи з Systemd, порівняно з попередніми методами, які використовувалися для керування службами, полягає в тому, що він забезпечує єдиний інтерфейс для запуску модулів.
Цей інтерфейс визначено у файлі модуля. Файли юнітів можуть розташовуватися в трьох місцях:

- */usr/lib/systemd/system* містить файли юнітів за замовчуванням, які були встановлені з пакетів RPM. Ви ніколи не повинні редагувати ці файли безпосередньо.
- */etc/systemd/system* містить файли, які були написані адміністратором або згенеровані командою ~systemctl edit~.
- */run/systemd/system* містить файли модулів, які були створені автоматично.

Пріоритет файлів: run → etc → usr/lib

** Юніт файли
*** Service
Мабуть, найважливішим типом підрозділу є сервісний юніт. Використовується для запуску процесів.
Ви можете запустити будь-який тип процесу за допомогою сервісного юніту.

#+begin_src conf
  [Unit]
  Description=Vsftpd ftp daemon
  After=network.target
  [Service]
  Type=forking
  ExecStart=/usr/sbin/vsftpd /etc/vsftpd/vsftpd.conf
  [Install]
  WantedBy=multi-user.target
#+end_src

Будь-який юніт файл складається з трьох розділів:
**** [Unit]
Описує юніт та визначає залежності.
Цей розділ також містить важливий оператор *After* і необов’язково оператор *Before*.
Ці оператори визначають залежності між різними юнітами.
Оператор *Before* вказує на те, що цей модуль має бути запущений перед вказаним модулем.
Оператор *After* вказує, що цей блок слід запускати після вказаного блоку.

**** [Service]
Описує, як запускати та зупиняти сервіс.
Зазвичай ви можете очікувати рядок *ExecStart*, який вказує, як запустити юніт, або рядок *ExecStop*, який вказує, як зупинити його.
Зверніть увагу на параметр *Type*, який використовується для вказівки того, як має початися процес.
Тип *forking* зазвичай використовується процесами-демонами, але ви також можете використовувати інші типи, такі як *oneshot*, який запускатиме будь-яку команду.
Додаткова інформація: ~man 5 systemd.service~.

**** [Install]
Вказує target для цього сервісу.
Цей розділ не інтерпретується systemd під час виконання; він використовується командами *enable* і *disable* інструменту ~systemctl~ під час інсталяції юніту.

*** Mount
Юніт монтування вказує, як файлова система може бути змонтована в певному каталозі.
#+begin_src conf
  [Unit]
  Description=Temporary Directory (/tmp)
  Documentation=man:hier(7)
  Documentation=https://www.freedesktop.org/wiki/Software/systemd/
  APIFileSystems
  ConditionPathIsSymbolicLink=!/tmp
  DefaultDependencies=no
  Conflicts=umount.target
  Before=local-fs.target umount.target
  After=swap.target
  [Mount]
  What=tmpfs
  Where=/tmp
  Type=tmpfs
  Options=mode=1777,strictatime,nosuid,nodev
#+end_src

Тут зʼявляються деякі додаткові опції, *[Unit]* - оператор *Conflicts* використовується для переліку юнітів, які не можна використовувати разом із цим. Використовуйте це для взаємовиключних юнітів. *[Mount]* - визначає, де саме має бути виконано кріплення. Ви впізнаєте аргументи, які зазвичай використовуються в команді ~mount~ та файлі */etc/fstab*. 

*** Socket
Ще один тип юнітів, на який цікаво подивитися, - це сокет.
Сокет створює спосіб для додатків спілкуватися один з одним.
Сокет можна визначити як файл, а також як порт, на якому Systemd буде прослуховувати вхідні з’єднання.
Таким чином, служба не повинна працювати безперервно, а натомість запускатиметься лише за умови встановлення з’єднання через вказаний сокет.
Кожен сокет потребує відповідного файлу сервіса.
#+begin_src conf
  [Unit]
  Description=Cockpit Web Service Socket
  Documentation=man:cockpit-ws(8)
  Wants=cockpit-motd.service
  [Socket]
  ListenStream=9090
  ExecStartPost=-/usr/share/cockpit/motd/update-motd '' localhost
  ExecStartPost=-/bin/ln -snf active.motd /run/cockpit/motd
  ExecStopPost=-/bin/ln -snf /usr/share/cockpit/motd/inactive.motd /run/cockpit/motd
  [Install]
  WantedBy=sockets.target
#+end_src

Важливою опцією є *ListenStream*.
Цей параметр визначає TCP-порт, який Systemd має прослуховувати вхідні з’єднання.
Сокети також можна створювати для UDP-портів, у цьому випадку слід використовувати *ListenDatagram* замість *ListenStream*.


Оскільки systemd надає дуже багато можливих опцій для конфігурації юнітів, корисним буде дізнатись всі можливі опції для типу з яким ви працюєте і їх значення завмовчки.
Для цього використовується команда ~systemd show~.

** Targets
Файли юнітів використовуються для створення функціональних можливостей, необхідних на вашому сервері.
Щоб мати можливість завантажити їх у потрібному порядку та в потрібний момент, використовується певний тип юніта: цільовий юніт (target unit).
Просте визначення цільового юніту – це «група юнітів».

Деякі цілі використовуються для визначення стану, в якому повинен бути запущений сервер.
Таким чином, цільові юніти можна порівняти з рівнями виконання, що використовуються в інших системах ініціалізації.

Інші таргети — це лише група служб, яка полегшує керування не лише окремими юнітами, але й усіма юнітами, які потрібні для отримання певної функціональності.

Цілі самі по собі можуть мати залежності від інших цілей.
Ці залежності визначені в цільовому юніті.
Прикладом такого відношення залежності є *basic.target*.
Цей таргет визначає всі юніти, які завжди повинні бути запущені.
Ви можете використовувати команду ~systemctl list-dependencies~ для огляду будь-яких існуючих
залежності.

Приклад: multiuser.target
#+begin_src conf
  [Unit]
  Description=Multi-User System
  Documentation=man:systemd.special(7)
  Requires=basic.target
  Conflicts=rescue.service rescue.target
  After=basic.target rescue.service rescue.target
  AllowIsolate=yes
  [Install]
  Alias=default.target
#+end_src

Ви можете змінити ціль з якою наразі запущена система за допомогою ~systemctl isolate~.


* Практика
- Встановіть служби vsftpd і httpd

#+begin_src conf
[root@fedora /]# dnf install vsftpd
[root@fedora /]# dnf install httpd
#+end_src

- Встановіть редактором systemctl за замовчуванням vim

#+begin_src conf
?????

[root@fedora system]# cd /home/fedora36server
nano: export SYSTEMD_EDITOR=vim
[root@fedora system]# visudo
nano: Defaults    env_keep += "SYSTEMD_EDITOR"

[fedora36server@fedora ~]$ cd /usr/lib/systemd/system
[fedora36server@fedora system]$ sudo systemctl edit httpd.service

-still nano-

?????
#+end_src

- Відредагуйте файл юніту httpd.service таким чином, щоб запуск httpd завжди автоматично запускав vsftpd. Відредагуйте службу httpd так, щоб після збою вона запускалася автоматично знову через 10 секунд.

#+begin_src conf
[fedora36server@fedora ~]$ cd /usr/lib/systemd/system
[fedora36server@fedora system]$ nano httpd.service
Wants=vsftpd.service
Restart=on-failure
RestartSec=10sec
[root@fedora sbin]# systemctl start httpd
[root@fedora sbin]# systemctl status httpd
● httpd.service - The Apache HTTP Server
     Loaded: loaded (/usr/lib/systemd/system/httpd.service; disabled; vendor preset: disabled)
     Active: active (running) since Mon 2022-08-29 00:11:12 EEST; 5s ago
       Docs: man:httpd.service(8)
   Main PID: 1216 (httpd)
     Status: "Started, listening on: port 80"
      Tasks: 177 (limit: 9480)
     Memory: 13.9M
        CPU: 207ms
     CGroup: /system.slice/httpd.service
             ├─ 1216 /usr/sbin/httpd -DFOREGROUND
             ├─ 1219 /usr/sbin/httpd -DFOREGROUND
             ├─ 1220 /usr/sbin/httpd -DFOREGROUND
             ├─ 1221 /usr/sbin/httpd -DFOREGROUND
             └─ 1222 /usr/sbin/httpd -DFOREGROUND

Aug 29 00:11:12 fedora systemd[1]: Starting httpd.service - The Apache HTTP Server...
Aug 29 00:11:12 fedora httpd[1216]: AH00558: httpd: Could not reliably determine the server's fully qualified domain na>
Aug 29 00:11:12 fedora httpd[1216]: Server configured, listening on: port 80
Aug 29 00:11:12 fedora systemd[1]: Started httpd.service - The Apache HTTP Server.
[root@fedora sbin]# systemctl status vsftpd
● vsftpd.service - Vsftpd ftp daemon
     Loaded: loaded (/usr/lib/systemd/system/vsftpd.service; disabled; vendor preset: disabled)
     Active: active (running) since Mon 2022-08-29 00:11:12 EEST; 17s ago
    Process: 1217 ExecStart=/usr/sbin/vsftpd /etc/vsftpd/vsftpd.conf (code=exited, status=0/SUCCESS)
   Main PID: 1218 (vsftpd)
      Tasks: 1 (limit: 9480)
     Memory: 896.0K
        CPU: 9ms
     CGroup: /system.slice/vsftpd.service
             └─ 1218 /usr/sbin/vsftpd /etc/vsftpd/vsftpd.conf

Aug 29 00:11:12 fedora systemd[1]: Starting vsftpd.service - Vsftpd ftp daemon...
Aug 29 00:11:12 fedora systemd[1]: Started vsftpd.service - Vsftpd ftp daemon.
#+end_src

- Створіть новий юніт файл, який буде записувати в /tmp файл чиєю назвою буде "mylovelyunit-date", дата має бути в форматі unix-timestamp.

#+begin_src conf
[root@fedora system]# [root@fedora system]# pwd
/usr/lib/systemd/system
[root@fedora system]# cat newunit.service
[Unit]
Description=makes a new file in /tmp

[Service]
ExecStart=/bin/bash /usr/sbin/newunit.sh

[Install]
WantedBy=multi-user.target

[root@fedora system]# nano /usr/sbin/newunit.sh
[root@fedora system]# cat /usr/sbin/newunit.sh
#!/bin/bash
touch /tmp/mylovelyunit-date
date '+%s' > /tmp/mylovelyunit-date

[root@fedora system]# systemctl daemon-reload
[root@fedora system]# systemctl enable newunit
[root@fedora system]# systemctl start newunit
[root@fedora system]# systemctl status newunit
○ newunit.service - makes a new file in /tmp
     Loaded: loaded (/usr/lib/systemd/system/newunit.service; enabled; vendor preset: disabled)
     Active: inactive (dead) since Mon 2022-08-29 00:51:54 EEST; 5s ago
    Process: 1848 ExecStart=/bin/bash /usr/sbin/newunit.sh (code=exited, status=0/SUCCESS)
   Main PID: 1848 (code=exited, status=0/SUCCESS)
        CPU: 21ms

Aug 29 00:51:54 fedora systemd[1]: Started newunit.service - makes a new file in /tmp.
Aug 29 00:51:54 fedora systemd[1]: newunit.service: Deactivated successfully.
[root@fedora system]# ls -l /tmp | grep mylovelyunit-date
-rw-r--r--. 1 root root 11 Aug 29 00:51 mylovelyunit-date
[root@fedora system]# cat /tmp/mylovelyunit-date
1661723514
#+end_src

- Додайте ще один юніт, який буде запускати юніт створений у попередньому завданні кожні 5 хвилин.

#+begin_src conf
?????

[root@fedora system]# pwd
/usr/lib/systemd/system
[root@fedora system]# nano newunittimer.timer
[root@fedora system]# cat newunittimer.timer
[Unit]
Description=reload new unit every 5 min
Requires=newunit.service

[Timer]
Unit=newunit.service
OnCalendar=*-*-* *:05:00

[Install]
WantedBy=timers.target

[root@fedora system]# systemctl status newunittimer.timer
○ newunittimer.timer - reload new unit every 5 min
     Loaded: loaded (/usr/lib/systemd/system/newunittimer.timer; enabled; vendor preset: disabled)
     Active: inactive (dead)
    Trigger: n/a
   Triggers: ● newunit.service

Aug 29 01:03:38 fedora systemd[1]: /usr/lib/systemd/system/newunittimer.timer:7: Failed to parse calendar specification, ignoring: *-*-* *:*/5:00
Aug 29 01:03:38 fedora systemd[1]: newunittimer.timer: Timer unit lacks value setting. Refusing.
Aug 29 01:12:32 fedora systemd[1]: /usr/lib/systemd/system/newunittimer.timer:7: Failed to parse calendar specification, ignoring: "*-*-* *:05:00"
Aug 29 01:12:32 fedora systemd[1]: newunittimer.timer: Timer unit lacks value setting. Refusing.
Aug 29 01:25:43 fedora systemd[1]: /usr/lib/systemd/system/newunittimer.timer:7: Failed to parse calendar specification, ignoring: '*-*-* *:05:00'
Aug 29 01:25:43 fedora systemd[1]: newunittimer.timer: Timer unit lacks value setting. Refusing.
Aug 29 01:26:00 fedora systemd[1]: /usr/lib/systemd/system/newunittimer.timer:7: Failed to parse calendar specification, ignoring: '*-*-* *:05:00'
Aug 29 01:26:00 fedora systemd[1]: newunittimer.timer: Timer unit lacks value setting. Refusing.
[root@fedora system]# cat newunittimer.timer

?????
#+end_src


* Посилання
- [[https://systemd-by-example.com/][systemd by example]]
- [[https://www.freedesktop.org/software/systemd/man/systemd.service.html][systemd.service]]
- [[https://www.freedesktop.org/software/systemd/man/systemd.unit.html][systemd.unit]]
