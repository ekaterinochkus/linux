* Управління дисками
** MBR & GPT
Щоб використовувати жорсткий диск, він повинен мати розділи.
Використання кількох розділів у системі має сенс з кількох причин:
- Легше розрізняти різні типи даних.
- Спеціальні параметри монтування можна використовувати для підвищення безпеки або продуктивності.
- Простіше створити стратегію резервного копіювання, у якій створюватимуться резервні копії лише відповідних частин ОС.

*** MBR
У MBR можна створити лише чотири розділи.
Оскільки багатьом операційним системам ПК потрібно більше чотирьох розділів, було знайдено рішення, яке вийшло за межі чотирьох розділів.
У MBR один розділ може бути створений як розширений розділ, на відміну від інших розділів, які були створені як основні розділи.
У розширеному розділі можна створити кілька логічних розділів, щоб досягти загальної кількості 15 розділів, до яких може звертатися ядро Linux.

MBR був визначений як перші 512 байт на жорсткому диску комп’ютера, і в MBR був присутній завантажувач операційної системи, а також таблиця розділів.
Розмір, який використовувався для таблиці розділів, був відносно малим, лише 64 байти, в результаті чого в MBR можна було створити не більше чотирьох розділів.
Оскільки дані про розмір розділу зберігалися в 32-розрядних значеннях і використовувався стандартний розмір сектора 512 байт, максимальний розмір, який міг використовувати розділ, був обмежений 2 ТіБ.

*** GPT
Сучасні жорсткі диски комп’ютерів стали занадто великими, щоб використовувати MBR.
Це одна з головних причин, чому потрібна була нова схема поділу.
На комп’ютерах, які використовують новий UEFI як заміну старої системи BIOS, розділи GPT є єдиним способом адресації дисків.
Крім того, старіші комп’ютерні системи, які використовують BIOS замість UEFI, можуть бути налаштовані за допомогою розділів GUID, що необхідно, якщо потрібно адресувати диск розміром більше 2 TiB.


** Розділи і файлові системи
Утиліта ~fdisk~ існує вже давно і використовується для створення розділів MBR. Утиліта ~gdisk~ використовується для створення розділів GPT.
Як для розділів MBR, так і для GPT вам потрібно вказати назву дискового пристрою як аргумент.

Якщо ви хочете вийти за межі чотирьох розділів на диску MBR, вам потрібно створити розширений розділ.
Після цього ви можете створювати логічні розділи в розширеному розділі.

Використання логічних розділів дозволяє вийти за рамки обмеження чотирьох розділів у MBR; але є і недолік.
Усі логічні розділи існують у розширеному розділі.
Якщо щось піде не так із розширеним розділом, у вас також є проблема з усіма наявними в ньому логічними розділами.

Якщо диск налаштовано за допомогою GPT або якщо це новий диск, який ще нічого не містить і має розмір, що перевищує 2 TiB, вам потрібно використовувати утиліту ~gdisk~ для створення розділів GUID.
Ця утиліта має багато схожості з ~fdisk~, але також має деякі відмінності.

*** Створюємо файлову систему
Розділ сам по собі не дуже корисний.
Він стане корисним лише тоді, коли ви вирішите з ним щось зробити.
Це часто означає, що ви повинні розмістити файлову систему поверх нього.

Щоб відформатувати розділ за допомогою однієї з підтримуваних файлових систем, ви можете скористатися командою ~mkfs~, використовуючи опцію *-t*, щоб вказати, яку файлову систему використовувати.
Крім того, для форматування файлової системи Ext4 можна використати один із спеціальних інструментів для файлової системи, наприклад ~mkfs.ext4~.

При роботі з файловими системами також можна керувати деякими властивостями.
Властивості файлової системи є специфічними для файлової системи, яку ви використовуєте, тому ви працюєте з різними властивостями та різними інструментами для різних файлових систем.

Загальним інструментом для керування властивостями файлової системи *Ext4* є ~tune2fs~.
Цей інструмент був розроблений для файлової системи *Ext2* і також сумісний з *Ext3* і *Ext4*.
Під час керування властивостями файлової системи *Ext4* починайте з ~tune2fs -l~.

Файлова система *XFS* — це зовсім інша файлова система, і з цієї причини також має зовсім інший набір інструментів для керування її властивостями.
Це не дозволяє встановлювати атрибути файлової системи в метаданих файлової системи.
Однак ви можете змінити деякі властивості *XFS* за допомогою команди ~xfs_admin~.
Наприклад, використовуйте ~xfs_admin -L mylabel~, щоб встановити мітку файлової системи на *mylabel*.

** Swap
Використання підкачки в Linux є зручним способом покращити використання пам’яті ядра Linux.
Якщо виникає нестача фізичної оперативної пам’яті, сторінки пам’яті, які не використовувалися нещодавно, можна перемістити для обміну, що робить більше оперативної пам’яті доступним для програм, яким потрібен доступ до сторінок пам’яті.
З цієї причини більшість серверів Linux налаштовано з певною кількістю свопу.
Якщо своп почне інтенсивно використовуватися, у вас можуть бути проблеми, тому слід уважно стежити за використанням свопу.

** Монтування файлових систем
Просто створити розділ і розмістити на ньому файлову систему недостатньо, щоб почати його використовувати.
Щоб використовувати розділ, його також потрібно змонтувати.

Для монтування файлової системи потрібна певна інформація:
- Що монтувати: ця інформація є обов’язковою та вказує назву пристрою, який потрібно монтувати.
- Де його монтувати: це також обов’язкова інформація, яка визначає каталог, до якого пристрій має бути змонтований.
- Яку файлову систему монтувати: за бажанням ви можете вказати тип файлової системи.
- Параметри монтування: під час монтування пристрою можна використовувати багато варіантів монтування.

Для монтування файлової системи вручну використовується команда ~mount~.
Щоб відключити змонтовану файлову систему, використовується команда ~umount~.

*** /etc/fstab
Зазвичай ви не бажаєте монтувати файлові системи вручну.
Якщо ви задоволені ними, гарною ідеєю буде їх автоматичне монтування.
Класичний спосіб зробити це за допомогою файлу */etc/fstab*.

* Терміни
MBR, GPT, UEFI, partition, primary partition, extended partition, LVM, mount, umount, ext4, xfs, fstab

* Практика
- Додайте два розділи до свого сервера. Створіть обидва розділи розміром 100 MiB. Один із цих розділів має бути налаштований як swap; інший розділ має бути відформатований у файловій системі Ext4.

#+begin_src bash 
[root@fedora /]# lsblk
NAME                   MAJ:MIN RM  SIZE RO TYPE MOUNTPOINTS
sda                      8:0    0   65G  0 disk
├─sda1                   8:1    0    1G  0 part /boot
└─sda2                   8:2    0   64G  0 part
  └─fedora_fedora-root 253:0    0   15G  0 lvm  /
sdb                      8:16   0  1.5G  0 disk
sdc                      8:32   0    1G  0 disk
sr0                     11:0    1 1024M  0 rom
zram0                  252:0    0  7.8G  0 disk [SWAP]
[root@fedora /]# fdisk /dev/sdb

Welcome to fdisk (util-linux 2.38).
Changes will remain in memory only, until you decide to write them.
Be careful before using the write command.


Command (m for help): g
Created a new GPT disklabel (GUID: 1025B12B-A485-D945-A078-6FB7AB82F4AC).

Command (m for help): n
Partition number (1-128, default 1):
First sector (2048-3145694, default 2048):
Last sector, +/-sectors or +/-size{K,M,G,T,P} (2048-3145694, default 3143679): +100M

Created a new partition 1 of type 'Linux filesystem' and of size 100 MiB.
Partition #1 contains a swap signature.

Do you want to remove the signature? [Y]es/[N]o: y

The signature will be removed by a write command.

Command (m for help): t
Selected partition 1
Partition type or alias (type L to list all): 19
Changed type of partition 'Linux filesystem' to 'Linux swap'.

Command (m for help): n
Partition number (2-128, default 2):
First sector (206848-3145694, default 206848):
Last sector, +/-sectors or +/-size{K,M,G,T,P} (206848-3145694, default 3143679): +100M

Created a new partition 2 of type 'Linux filesystem' and of size 100 MiB.
Partition #2 contains a ext4 signature.

Do you want to remove the signature? [Y]es/[N]o: y

The signature will be removed by a write command.

Command (m for help): w
The partition table has been altered.
Calling ioctl() to re-read partition table.
Syncing disks.

[root@fedora /]# lsblk
NAME                   MAJ:MIN RM  SIZE RO TYPE MOUNTPOINTS
sda                      8:0    0   65G  0 disk
├─sda1                   8:1    0    1G  0 part /boot
└─sda2                   8:2    0   64G  0 part
  └─fedora_fedora-root 253:0    0   15G  0 lvm  /
sdb                      8:16   0  1.5G  0 disk
├─sdb1                   8:17   0  100M  0 part
└─sdb2                   8:18   0  100M  0 part
sdc                      8:32   0    1G  0 disk
sr0                     11:0    1 1024M  0 rom
zram0                  252:0    0  7.8G  0 disk [SWAP]
[root@fedora /]# mkfs.ext4 /dev/sdb2
mke2fs 1.46.5 (30-Dec-2021)
Creating filesystem with 102400 1k blocks and 25584 inodes
Filesystem UUID: c17e579a-163b-46ad-88e4-c442d7e23944
Superblock backups stored on blocks:
        8193, 24577, 40961, 57345, 73729

Allocating group tables: done
Writing inode tables: done
Creating journal (4096 blocks): done
Writing superblocks and filesystem accounting information: done

[root@fedora /]# mkswap /dev/sdb1
Setting up swapspace version 1, size = 100 MiB (104853504 bytes)
no label, UUID=ca21c418-cb67-4693-8cfb-f57cc1a9f01b
[root@fedora /]# free
               total        used        free      shared  buff/cache   available
Mem:         8133460      259408     7607112        1036      266940     7639616
Swap:        8132604           0     8132604
[root@fedora /]# swapon /dev/sdb1
[root@fedora /]# free
               total        used        free      shared  buff/cache   available
Mem:         8133460      259376     7607112        1036      266972     7639644
Swap:        8235000           0     8235000

[root@fedora /]# mount /dev/sdb2 /run/sdb2drive
[root@fedora /]# lsblk
NAME                   MAJ:MIN RM  SIZE RO TYPE MOUNTPOINTS
sda                      8:0    0   65G  0 disk
├─sda1                   8:1    0    1G  0 part /boot
└─sda2                   8:2    0   64G  0 part
  └─fedora_fedora-root 253:0    0   15G  0 lvm  /
sdb                      8:16   0  1.5G  0 disk
├─sdb1                   8:17   0  100M  0 part [SWAP]
└─sdb2                   8:18   0  100M  0 part /run/sdb2drive
sdc                      8:32   0    1G  0 disk
sr0                     11:0    1 1024M  0 rom
zram0                  252:0    0  7.8G  0 disk [SWAP]
#+end_src

- Налаштуйте свій сервер на автоматичне монтування цих розділів. Змонтуйте розділ Ext4 на /data та змонтуйте розділ підкачки як простір підкачки.

#+begin_src bash 
[root@fedora /]# blkid
/dev/sdb2: UUID="c17e579a-163b-46ad-88e4-c442d7e23944" BLOCK_SIZE="1024" TYPE="ext4" PARTUUID="9a0966c7-218c-eb42-8966-9eceff247f06"
/dev/sdb1: UUID="ca21c418-cb67-4693-8cfb-f57cc1a9f01b" TYPE="swap" PARTUUID="02aef625-9df0-e04c-b042-bf69614a971b"
/dev/mapper/fedora_fedora-root: UUID="3e8aec28-1997-49fe-b129-c1cdc111ffcf" BLOCK_SIZE="512" TYPE="xfs"
/dev/sda2: UUID="CsyMen-ODAr-SKsT-NhLk-U2UB-tGTL-SRtyWL" TYPE="LVM2_member" PARTUUID="4c5db969-02"
/dev/sda1: UUID="23b74018-1103-4993-bebb-a391f7678c5c" BLOCK_SIZE="512" TYPE="xfs" PARTUUID="4c5db969-01"
/dev/zram0: LABEL="zram0" UUID="17de1cae-60ed-4b35-a795-d7c5991dce95" TYPE="swap"
[root@fedora /]# echo "UUID=c17e579a-163b-46ad-88e4-c442d7e23944" >> /etc/fstab
[root@fedora /]# nano /etc/fstab
[root@fedora /]# echo "UUID=ca21c418-cb67-4693-8cfb-f57cc1a9f01b" >> /etc/fstab
[root@fedora /]# nano /etc/fstab
[root@fedora /]# cat /etc/fstab
#
# /etc/fstab
# Created by anaconda on Tue Jul 19 15:03:57 2022
#
# Accessible filesystems, by reference, are maintained under '/dev/disk/'.
# See man pages fstab(5), findfs(8), mount(8) and/or blkid(8) for more info.
#
# After editing this file, run 'systemctl daemon-reload' to update systemd
# units generated from this file.
#
/dev/mapper/fedora_fedora-root /                       xfs     defaults        0 0
UUID=23b74018-1103-4993-bebb-a391f7678c5c /boot                   xfs     defaults        0 0
UUID=c17e579a-163b-46ad-88e4-c442d7e23944 /run/sdb2drive ext4 rw,relatime 0 2
UUID=ca21c418-cb67-4693-8cfb-f57cc1a9f01b none swap defaults 0 0

#+end_src

- Перезавантажте сервер і переконайтеся, що все змонтовано правильно.

#+begin_src bash 
Last login: Mon Aug 15 14:11:27 2022
[fedora36server@fedora ~]$ su
Password:
[root@fedora fedora36server]# cd /
[root@fedora /]# lsblk
NAME                   MAJ:MIN RM  SIZE RO TYPE MOUNTPOINTS
sda                      8:0    0   65G  0 disk
├─sda1                   8:1    0    1G  0 part /boot
└─sda2                   8:2    0   64G  0 part
  └─fedora_fedora-root 253:0    0   15G  0 lvm  /
sdb                      8:16   0  1.5G  0 disk
├─sdb1                   8:17   0  100M  0 part [SWAP]
└─sdb2                   8:18   0  100M  0 part /run/sdb2drive
sdc                      8:32   0    1G  0 disk
sr0                     11:0    1 1024M  0 rom
zram0                  252:0    0  7.8G  0 disk [SWAP]
[root@fedora /]#
#+end_src
