* kernel
Ядро Linux є серцем операційної системи.
Це прошарок між користувачем, який працює з Linux у середовищі оболонки, та апаратним забезпеченням, доступним на комп’ютері, на якому працює користувач.

Ядро керує інструкціями введення-виведення, які воно отримує від програмного забезпечення, і перетворює їх на інструкції, які виконуються процесором та іншим апаратним забезпеченням комп’ютера.

Ядро також виконує основні завдання операційної системи.
Одним із прикладів такого завдання є планувальник, який гарантує, що будь-які процеси, запущені в операційній системі, обробляються ЦП.


Завдання операційної системи, які виконує ядро, реалізуються різними потоками ядра.
Потоки ядра легко розпізнати за допомогою такої команди, як ~ps aux~.
Назви потоків ядра вказані в квадратних дужках.

#+begin_src sh
  $ ps aux | head -n10
  USER         PID %CPU %MEM    VSZ   RSS TTY     STAT START   TIME COMMAND
  root           2  0.0  0.0      0     0 ?       S    16:42   0:00 [kthreadd]
  root           3  0.0  0.0      0     0 ?       I<   16:42   0:00 [rcu_gp]
  root           4  0.0  0.0      0     0 ?       I<   16:42   0:00 [rcu_par_gp]
  ...
#+end_src

Іншим важливим завданням ядра Linux є апаратна ініціалізація.
Щоб переконатися, що це обладнання можна використовувати, ядро Linux використовує драйвери.
Кожна частина апаратного забезпечення містить певні функції, і щоб використовувати ці функції, потрібно завантажити драйвер.
Ядро Linux є модульним, а драйвери завантажуються як модулі ядра.


Щоб допомогти проаналізувати, що робить ядро, Linux існують такі інструменти:
- ~dmesg~
- файлова система */proc*
- утіліта ~uname~

Утиліта ~dmesg~ показує вміст кільцевого буфера ядра, області пам’яті, де ядро Linux зберігає останні повідомлення журналу.
Альтернативним способом отримання доступу до тієї самої інформації в кільцевому буфері ядра є команда ~journalctl --dmesg~, яка еквівалентна ~journalctl -k~.


#+begin_src
# dmesg | tail -n10
[  277.882259] virbr1: port 1(vnet1) entered listening state
[  277.990145] kvm: SMP vm created on host with unstable TSC; guest TSC will not be reliable
[  280.033614] virbr1: port 1(vnet1) entered learning state
[  280.033625] virbr0: port 1(vnet0) entered learning state
[  282.166940] virbr1: port 1(vnet1) entered forwarding state
...
#+end_src

У виводі ~dmesg~ відображаються всі повідомлення, пов’язані з ядром.
Кожне повідомлення починається з індикатора часу (відносно початку завантаження ядра), який показує, у яку конкретну секунду було зареєстровано подію.
Цей індикатор часу пов’язаний із запуском ядра, що дозволяє вам точно побачити, скільки секунд пройшло між запуском ядра та певною подією. Для відображення часу в абсолютних значеннях використовуйте ~dmesg -T~.


Ще одним цінним джерелом інформації є файлова система */proc*.
Файлова система */proc* є інтерфейсом до ядра Linux і містить файли з детальною інформацією про стан того, що відбувається на вашому сервері.
Багато інструментів, пов’язаних із продуктивністю роботи компʼютера, використовують файлову систему */proc* для отримання інформації про стан системи.

Деякі файли в */proc* доступні для читання людиною та містять інформацію про стан процесора, пам'яті, монтувань тощо.
Подивіться, наприклад, на */proc/meminfo* або */pric/cpuinfo*, де надається детальна інформація про памʼять та процесор.


Останнім корисним джерелом інформації є команда ~uname~.
Ця команда надає різну інформацію про вашу операційну систему.
Введіть, наприклад, ~uname -a~ для виводу всієї інформації, ~uname -r~, щоб побачити, яка зараз використовується версія ядра.
Ця інформація також відображається під час використання команди ~hostnamectl status~.

#+begin_src 
$ hostnamectl status
...
Machine ID: 92305e61f07d42ab8cabfde984fde31a
Boot ID: 8c7952d29f3944a997de03f2bd9bb294
Operating System: Arch Linux
Kernel: Linux 5.19.7-arch1-1
Architecture: x86-64
...
#+end_src

** kernel modules
Модулі ядра реалізують певні функції.
Модулі ядра використовуються для завантаження драйверів, які забезпечують коректне функціонування апаратних пристроїв, але не обмежуються лише завантаженням драйверів апаратного забезпечення.
Наприклад, підтримка файлових систем завантажується як модулі.
Інші функції ядра також можна завантажити як модулі.

Як проходить ініціалізація пристроїв (поверхнево):
- Під час завантаження ядро отримує список доступного обладнання.
- Після виявлення пристроїв *systemd-udevd* завантажує відповідний драйвер і робить апаратний пристрій доступним.
- Щоб визначити спосіб ініціалізації пристроїв, *systemd-udevd* читає файли правил у */usr/lib/udev/rules.d*. Це правила які не бажано змінювати.
- Після обробки наданих системою файлів правил *udev* systemd-udevd переходить до каталогу */etc/udev/rules.d*, щоб прочитати будь-які локальні правила, якщо вони є.
- У результаті необхідні модулі ядра завантажуються автоматично, а статус модулів ядра та відповідного обладнання записується у файлову систему sysfs, який змонтовано в каталозі */sys*. Ядро Linux використовує цю псевдофайлову систему для відстеження апаратних налаштувань.

Процес *systemd-udevd* не є одноразовим; він постійно відстежує підключення та відключення нових апаратних пристроїв.
Щоб отримати уявлення про те, як це працює, ви можете ввести команду ~udevadm monitor~ від користувача _root_.

Альтернативний спосіб завантаження модулів ядра — через каталог */etc/modules-load.d*.
У цьому каталозі ви можете створювати файли для автоматичного завантаження модулів.
Для модулів за замовчуванням, які мають завжди завантажуватися, це каталог */usr/lib/modules-load.d*.


*** Ручне керування модулями ядра
|-------------+------------------------------------------|
| Команда     | Опис                                     |
|-------------+------------------------------------------|
| lsmod       | Виводить завантажені модулі ядра         |
|-------------+------------------------------------------|
| modinfo     | Відображає інформацію про модуль ядра    |
|-------------+------------------------------------------|
| modprobe    | Завантажує модуль ядра та залежні модулі |
|-------------+------------------------------------------|
| modprobe -r | Вивантажує модулі ядра                   |
|-------------+------------------------------------------|


* Практика
- Дізнайтеся, чи доступна нова версія ядра. Якщо так, інсталюйте його та перезавантажте комп’ютер, щоб воно могло використовуватися.

#+begin_src 
[root@fedora fedora36server]# hostnamectl status
   Static hostname: n/a
Transient hostname: fedora
         Icon name: computer-vm
           Chassis: vm 🖴
        Machine ID: 58eaf765b8384363ba294140059e49c2
           Boot ID: c77244894f4b47f89c0bd463614fd244
    Virtualization: oracle
  Operating System: Fedora Linux 36 (Server Edition)
       CPE OS Name: cpe:/o:fedoraproject:fedora:36
            Kernel: Linux 5.18.11-200.fc36.x86_64
      Architecture: x86-64
   Hardware Vendor: innotek GmbH
    Hardware Model: VirtualBox
[root@fedora fedora36server]# cd /
[root@fedora /]# dnf update
Fedora 36 - x86_64 - Updates                                                            9.1 kB/s |  13 kB     00:01
Fedora 36 - x86_64 - Updates                                                            917 kB/s | 3.6 MB     00:04
Fedora Modular 36 - x86_64 - Updates                                                     24 kB/s |  17 kB     00:00
Fedora Modular 36 - x86_64 - Updates                                                    138 kB/s | 118 kB     00:00
Last metadata expiration check: 0:00:02 ago on Thu 15 Sep 2022 03:34:57 PM EEST.
Dependencies resolved.
======================================================================================================================== Package                                    Architecture     Version                            Repository         Size
========================================================================================================================Installing:
 kernel                                     x86_64           5.19.8-200.fc36                    updates           262 k
...

...
Installed:
  amd-gpu-firmware-20220815-138.fc36.noarch   freetype-2.12.1-2.fc36.x86_64 graphite2-1.3.14-9.fc36.x86_64     grub2-tools-efi-1:2.06-53.fc36.x86_64 grub2-tools-extra-1:2.06-53.fc36.x86_64 harfbuzz-4.0.0-2.fc36.x86_64
  intel-gpu-firmware-20220815-138.fc36.noarch kernel-5.19.8-200.fc36.x86_64 kernel-core-5.19.8-200.fc36.x86_64 kernel-modules-5.19.8-200.fc36.x86_64 mtools-4.0.40-1.fc36.x86_64             nvidia-gpu-firmware-20220815-138.fc36.noarch
  unbound-anchor-1.16.2-3.fc36.x86_64

Complete!

C:\Users\User>ssh fedora36server@192.168.68.107
fedora36server@192.168.68.107's password:
Web console: https://fedora:9090/ or https://192.168.68.107:9090/

Last login: Thu Sep 15 15:20:54 2022 from 192.168.68.100
[fedora36server@fedora ~]$ hostnamectl status
   Static hostname: n/a
Transient hostname: fedora
         Icon name: computer-vm
           Chassis: vm 🖴
        Machine ID: 58eaf765b8384363ba294140059e49c2
           Boot ID: 9e69180edadd446d8111ae55d795c845
    Virtualization: oracle
  Operating System: Fedora Linux 36 (Server Edition)
       CPE OS Name: cpe:/o:fedoraproject:fedora:36
            Kernel: Linux 5.19.8-200.fc36.x86_64
      Architecture: x86-64
   Hardware Vendor: innotek GmbH
    Hardware Model: VirtualBox
[fedora36server@fedora ~]$
#+end_src

- Використайте відповідну команду, щоб показати останні події, зареєстровані ядром.

#+begin_src 
[root@fedora /]# dmesg -T | tail -10
[Thu Sep 15 15:52:42 2022] EXT4-fs (sdb2): mounted filesystem with ordered data mode. Quota mode: none.
[Thu Sep 15 15:52:42 2022] audit: type=1130 audit(1663246361.994:151): pid=1 uid=0 auid=4294967295 ses=4294967295 subj=system_u:system_r:init_t:s0 msg='unit=dracut-shutdown comm="systemd" exe="/usr/lib/systemd/systemd" hostname=? addr=? terminal=? res=success'
[Thu Sep 15 15:52:42 2022] audit: type=1130 audit(1663246362.036:152): pid=1 uid=0 auid=4294967295 ses=4294967295 subj=system_u:system_r:init_t:s0 msg='unit=systemd-boot-update comm="systemd" exe="/usr/lib/systemd/systemd" hostname=? addr=? terminal=? res=success'
[Thu Sep 15 15:52:43 2022] RPC: Registered named UNIX socket transport module.
[Thu Sep 15 15:52:43 2022] RPC: Registered udp transport module.
[Thu Sep 15 15:52:43 2022] RPC: Registered tcp transport module.
[Thu Sep 15 15:52:43 2022] RPC: Registered tcp NFSv4.1 backchannel transport module.
[Thu Sep 15 15:52:46 2022] NET: Registered PF_QIPCRTR protocol family
[Thu Sep 15 15:52:50 2022] e1000: enp0s3 NIC Link is Up 1000 Mbps Full Duplex, Flow Control: RX
[Thu Sep 15 15:52:50 2022] IPv6: ADDRCONF(NETDEV_CHANGE): enp0s3: link becomes ready
#+end_src

- Знайдіть модуль ядра, який предоставляє файлову систему в просторі користувача. Дізнайтеся, чи є у нього опції. Спробуйте завантажити одну опцію вручну; якщо це вдасться, вживіть необхідних заходів для постійного завантаження цього параметра.

#+begin_src 
[fedora36server@fedora ~]$ lsmod | grep fuse
fuse                  172032  1
[fedora36server@fedora ~]$ modinfo fuse
filename:       /lib/modules/5.19.8-200.fc36.x86_64/kernel/fs/fuse/fuse.ko.xz
alias:          devname:fuse
alias:          char-major-10-229
alias:          fs-fuseblk
alias:          fs-fuse
license:        GPL
description:    Filesystem in Userspace
author:         Miklos Szeredi <miklos@szeredi.hu>
alias:          fs-fusectl
depends:
retpoline:      Y
intree:         Y
name:           fuse
vermagic:       5.19.8-200.fc36.x86_64 SMP preempt mod_unload
sig_id:         PKCS#7
signer:         Fedora kernel signing key
sig_key:        2F:6C:63:F3:81:5C:63:2C:D1:16:B8:FE:2A:C8:85:C5:C8:C7:61:B8
sig_hashalgo:   sha256
signature:      83:D3:99:BB:08:3D:A1:3A:8C:90:28:5F:A9:F6:FB:9C:AD:EF:D5:66:
                62:F3:C5:30:1F:D8:CD:F9:02:69:21:13:D7:AF:07:36:60:DE:60:01:
                52:3B:B0:42:F7:7C:15:8B:21:2C:42:50:D3:21:C3:06:08:B9:7D:33:
                D4:2C:A2:5D:81:73:CA:D5:E9:54:14:70:1E:8B:84:55:82:D9:C0:D4:
                42:B3:4D:09:C4:D6:E1:2E:94:D9:F1:4A:13:D1:61:B5:D7:57:5C:D8:
                DD:0A:40:66:18:FD:00:A0:A7:F1:2B:40:8C:B7:10:25:AA:D4:AC:E1:
                96:9C:FA:6F:34:40:CF:06:32:07:3B:06:A5:E7:1B:2A:E2:E6:B2:B0:
                13:67:C5:BF:0B:29:F1:40:A0:2F:90:16:74:1D:CE:19:5E:D0:BD:C6:
                35:A5:DA:BB:47:40:00:3E:A6:59:59:88:DF:31:3D:CC:B3:84:74:36:
                50:25:1E:74:84:7C:46:D9:74:16:E8:98:EE:A1:B5:F5:7B:F0:CB:C0:
                5E:C9:A7:BA:BE:97:28:DA:7F:8B:9F:26:3D:1E:FA:E2:D0:9D:8D:CC:
                B4:A1:C0:86:EA:87:96:8E:56:0E:34:E3:8C:C9:3C:66:79:E6:CB:55:
                93:21:9A:D6:F5:B6:FB:5E:DF:80:B8:FF:04:6A:79:08:74:76:5F:2F:
                06:22:3A:40:DC:FF:CA:4C:7F:1F:F2:2B:D1:90:FF:83:E0:E6:19:4B:
                3D:E4:E2:24:AE:CE:CA:7F:63:FF:64:96:12:E1:7F:0A:71:BD:DD:AE:
                43:D2:03:9A:E6:CA:32:5A:4A:BA:D2:81:6F:85:4A:6E:81:48:BA:79:
                36:67:84:31:FF:50:5A:26:54:3B:20:04:1F:55:EB:20:6F:F0:CE:8C:
                96:17:EA:98:43:F9:A8:F9:16:61:8A:0A:02:E8:7B:5F:83:D0:A6:84:
                AE:81:A3:79:A9:63:43:10:7F:90:7F:59:0B:12:2F:C8:87:42:B2:24:
                6E:7A:C8:7D:B4:4B:5A:A0:DE:93:4F:4F:B9:6C:A8:28:74:BB:D7:B3:
                E8:A4:89:F3:40:AF:DD:1E:ED:A0:BF:AD:0C:BB:6B:41:80:3A:71:F7:
                D4:E5:42:80:C8:12:A9:2F:E0:1C:D5:06:EA:A5:25:6E:C1:21:3E:AC:
                B6:0C:A4:2C:E2:98:B6:5F:61:95:34:9B:45:D4:7B:E2:8E:ED:07:3B:
                BC:5B:05:35:B3:74:3A:2B:16:C4:7B:A7:EC:1A:51:15:95:48:A0:F0:
                3F:44:48:BA:2E:38:41:A8:D9:F4:03:3E:9B:DE:92:A5:B2:FC:77:FE:
                C0:06:F1:07:A5:52:1F:46:45:99:40:59
parm:           max_user_bgreq:Global limit for the maximum number of backgrounded requests an unprivileged user can set (uint)
parm:           max_user_congthresh:Global limit for the maximum congestion threshold an unprivileged user can set (uint)

[root@fedora fedora36server]# cd /etc/modules-load.d
[root@fedora modules-load.d]# ls
autorun.conf
[root@fedora modules-load.d]# cat autorun.conf
fuse max_user_congthresh=5000
[root@fedora modules-load.d]# modprobe fuse 

[root@fedora parameters]# ls
max_user_bgreq  max_user_congthresh
[root@fedora parameters]# cat max_user_congthresh
2594

#+end_src
